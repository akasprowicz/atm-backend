class InvalidArgumentException extends Error {
  constructor(value) {
    super(value);
    this.message = `Invalid value '${value}'`;
    this.name = 'InvalidArgumentException';
    this.toString = function() {
      return `[${this.name}] ${this.message}`;
    };
  }
}

class NoteUnavailableException extends Error {
  constructor(value) {
    super(value);
    this.message = `Notes unavailable for value '${value}'`;
    this.name = 'NoteUnavailableException';
    this.toString = function() {
      return `[${this.name}] ${this.message}`;
    };
  }
}

module.exports = {
  InvalidArgumentException,
  NoteUnavailableException
};
