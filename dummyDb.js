const atmNotes = [
  {
    value: 100.00,
    amount: Infinity
  },
  {
    value: 50.00,
    amount: Infinity
  },
  {
    value: 20.00,
    amount: Infinity
  },
  {
    value: 10.00,
    amount: Infinity
  }
];

const clientBalance = Infinity;

module.exports = {
  atmNotes,
  clientBalance
};
