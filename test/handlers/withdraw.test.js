const { withdrawHandler } = require('../../handlers/withdraw');
const {
  InvalidArgumentException,
  NoteUnavailableException
} = require('../../utils/exceptions');

describe('withdraw handler', () => {
  const jsonMock = jest.fn();

  afterEach(() => {
    jsonMock.mockClear();
  });

  it('given amount 50 should return [50]', () => {
    const req = {
      params: {
        amount: 50
      }
    };
    const res = {
      json: jsonMock
    }
    withdrawHandler(req, res);
    expect(jsonMock).toHaveBeenCalledWith([50]);
  });

  it('given amount 120 should return [100, 20]', () => {
    const jsonMock = jest.fn();
    withdrawHandler({
      params: {
        amount: 120
      }
    }, {
      json: jsonMock
    });
    expect(jsonMock).toHaveBeenCalledWith([100, 20]);
  });

  it('given amount 280 should return [100, 100, 50, 20, 10]', () => {
    const jsonMock = jest.fn();
    withdrawHandler({
      params: {
        amount: 280
      }
    }, {
      json: jsonMock
    });
    expect(jsonMock).toHaveBeenCalledWith([100, 100, 50, 20, 10]);
  });

  it('given amount NULL should return []', () => {
    const jsonMock = jest.fn();
    withdrawHandler({
      params: {
        amount: null
      }
    }, {
      json: jsonMock
    });
    expect(jsonMock).toHaveBeenCalledWith([]);
  });

  it('given amount -100 should throw InvalidArgumentException', () => {
    const jsonMock = jest.fn();
    try {
      withdrawHandler({
        params: {
          amount: -100
        }
      }, {
        json: jsonMock
      });
    } catch (err) {
      expect(err).toBeInstanceOf(InvalidArgumentException);
    }
  });

  it('given amount 101 should throw NoteUnavailableException', () => {
    const jsonMock = jest.fn();
    try {
      withdrawHandler({
        params: {
          amount: 101
        }
      }, {
        json: jsonMock
      });
    } catch (err) {
      expect(err).toBeInstanceOf(NoteUnavailableException);
    }
  });

});
