# ATM App - Back-end API

Please copy this repository and run following commands in the terminal:

```
npm install

npm start
```

The ATM App will start and be available on the address: http://localhost:3001

# Test

```
npm run test
```

# Details

Normally I would use [PM2](https://pm2.io) to initialize and keep alive
our Node.js process. It's a great tool with many configuration options.
Yet, it requires installing it as a global package (`-g` flag).
For this app to keep things simple we will use just Node to initialize the server.
