const {
  InvalidArgumentException,
  NoteUnavailableException
} = require('../utils/exceptions');
const { config } = require('../config');
const { atmNotes, clientBalance } = require('../dummyDb');

function withdrawHandler(req, res) {
  const withdrawAmount = req.params.amount || 0;

  if (isNaN(withdrawAmount)){
    throw new InvalidArgumentException(withdrawAmount);
  }

  let withdrawAmountParsed = parseInt(withdrawAmount);

  if (withdrawAmountParsed < 0){
    throw new InvalidArgumentException(withdrawAmount);
  }

  if (withdrawAmountParsed > clientBalance){
    throw new InvalidArgumentException(withdrawAmount);
  }

  const sortedAtmNotes = atmNotes
    .sort((a,b) => b.value - a.value); // sort by value desc

  const notes = [];
  sortedAtmNotes.map(notesInAtm => {
    let amount = Math.floor(withdrawAmountParsed/notesInAtm.value);
    if (notesInAtm.amount < amount) {
      amount = notesInAtm.amount;
    }
    while (amount > 0){
      amount--;
      notes.push(notesInAtm.value);
      notesInAtm.amount--;
      withdrawAmountParsed -= notesInAtm.value;
    }
    return notesInAtm;
  });

  if (withdrawAmountParsed > 0) {
    throw new NoteUnavailableException(withdrawAmount);
  }

  res.json(notes);
}

module.exports = {
  withdrawHandler
}
