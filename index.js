const express = require('express');
const cors = require('cors');

const { config } = require('./config');

const { withdrawHandler } = require('./handlers/withdraw');

const app = express();
app.use(cors());

/* Handlers */

app.get('/withdraw/:amount?', (req, res) => {
  withdrawHandler(req, res);
});

app.listen(config.port, () => console.log(`Example app listening on port ${config.port}!`));
